#!/usr/bin/env bash
#
# Deply files using git-ftp https://github.com/git-ftp/git-ftp
#
# Required globals:
#   HOST
#   REMOTE_PATH
#   USER
#   PASSWORD
#

set -e

blue="\\e[36m"
red="\\e[31m"
green="\\e[32m"
reset="\\e[0m"

# Logging, loosely based on http://www.ludovicocaldara.net/dba/bash-tips-4-use-logging-levels/
info() { echo -e "${blue}INFO: $*${reset}"; }
error() { echo -e "${red}ERROR: $*${reset}"; }
success() { echo -e "${green}✔ $*${reset}"; }
fail() { echo -e "${red}✖ $*${reset}"; }

if [[ -z "${SERVER}" ]]; then
  fail "SERVER variable is missing"
fi
if [[ -z "${USER}" ]]; then
  fail "USER variable is missing"
fi
if [[ -z "${PASSWORD}" ]]; then
  fail "PASSWORD variable is missing"
fi

if [ -z "${SERVER}" ] || [ -z "${USER}" ] || [ -z "${PASSWORD}" ]; then
  exit 1
fi

run_pipe() {
  info "Starting GIT-FTP deployment to ${SERVER}:${REMOTE_PATH}..."
  echo "git ftp push --auto-init --insecure --user ${USER} --passwd **** sftp://${SERVER}/${REMOTE_PATH}/"

  git ftp push --auto-init --insecure --user ${USER} --passwd ${PASSWORD} sftp://${SERVER}/${REMOTE_PATH}/ 2>&1
  status=$?

  if [[ "${status}" == "0" ]]; then
    success "Deployment finished."
  else
    fail "Deployment failed."
  fi

  exit $status
}

run_pipe
