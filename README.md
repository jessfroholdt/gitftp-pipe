# Bitbucket Pipelines Pipe: gitftp-pipe

Deploy to remote server using git-ftp.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: jessfroholdt/gitftp-pipe
  variables:
    SERVER: '<string>'
    REMOTE_PATH: '<string>'
    USER: '<string>'
    PASSWORD: '<string>'
```

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| SERVER (*)            | The remote host to transfer the files to. |
| REMOTE_PATH (*)       | The remote path to deploy files to. |
| USER (*)              | The user on the remote host to connect as. |
| PASSWORD (*)        | The password of the user. |

_(*) = required variable._

## Details

https://github.com/git-ftp/git-ftp
